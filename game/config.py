from random import randint
from typing import List, Optional

from pkg_resources import resource_stream
from yaml import load, SafeLoader


class GameObject:

    def __init__(self, name: Optional[str] = None):
        self.name = name or f'{self.__class__.__name__}-{randint(10000, 100000)}'

    @classmethod
    def load(cls, path: Optional[str] = None) -> List['GameObject']:
        if path:
            with open(path) as f:
                data = load(f, Loader=SafeLoader)
        else:
            with resource_stream('game', f'config/{cls.__name__.lower()}.yml') as f:
                data = load(f, Loader=SafeLoader)
        return [cls(**entry) for entry in data]

    def __repr__(self):
        args = ', '.join([f'{key}={repr(value)}' for key, value in self.__dict__.items()])
        return f'{self.__class__.__name__}({args})'
