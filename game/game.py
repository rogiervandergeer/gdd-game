from typing import List

from game.player import Player


class Game:

    def __init__(self, n_players: int):
        self.players: List[Player] = [Player() for _ in range(n_players)]
        self.turns: int = 0

    def play(self) -> 'Game':
        while not any([player.finished for player in self.players]):
            self.turns += 1
            for player in self.players:
                player.turn()
        return self

    @property
    def winner(self) -> Player:
        return max(self.players, key=lambda p: p.score)
