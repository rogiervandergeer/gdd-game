from typing import Any, Callable, List, Optional

from matplotlib.pyplot import subplots, Axes
from numpy import mean

from game.game import Game
from game.player import Player


def run_simulation(n: int = 1000, n_players: int = 4):
    games = [Game(n_players).play() for _ in range(n)]
    fig, axes = subplots(ncols=3, nrows=4, figsize=(16, 12))
    plot_turns(ax=axes[0][0], games=games)
    plot_player_count(axes[0][1], games, 'budget_cards', range(10))
    plot_player_prop(axes[1][0], games, 'budget', bins=range(0, 200, 5))
    plot_player_prop(axes[1][1], games, 'value', bins=range(0, 2000, 50))
    plot_player_prop(axes[1][2], games, 'team', bins=range(0, 10))
    plot_player_count(axes[2][0], games, 'increments', bins=range(0, 150, 5))
    plot_player_count(axes[0][2], games, 'drop', bins=range(10))
    plot_player_func(axes[2][1], games, lambda player: sum(uc.immediate_value for uc in player.completed),
                     'base_value', bins=range(0, 2000, 50))
    plot_player_func(axes[2][2], games, lambda player: sum(uc.handicap for uc in player.completed),
                     'sum_handicap', bins=range(0, 25))
    plot_player_func_multi(axes[3][0], games, lambda player: [uc.handicap for uc in player.completed],
                           bins=range(10), name='completed handicaps')
    plot_player_func_multi(axes[3][1], games, lambda player: [uc.handicap for uc in player.backlog],
                           bins=range(10), name='backlog handicaps')
    plot_player_func(axes[3][2], games, lambda player: len(player.completed), 'n_completed', bins=range(5))
    fig.tight_layout()


def plot_turns(ax: Axes, games: List[Game]):
    ax.hist([game.turns for game in games], bins=range(10, 150, 20))
    ax.set_xlabel('# turns')
    ax.set_ylabel('# games')
    ax.set_title(f'turns per game (mean:{mean([game.turns for game in games])})')


def plot_player_prop(ax: Axes, games: List[Game], attr: str, bins: Optional[range] = None):
    ax.hist([getattr(player, attr) for game in games for player in game.players], bins=bins, density=True)
    ax.hist([getattr(game.winner, attr) for game in games], bins=bins, color='r', density=True, alpha=0.5)
    ax.set_xlabel(attr)
    ax.set_title(attr)


def plot_player_count(ax: Axes, games: List[Game], key: str, bins: Optional[range] = None):
    ax.hist([player.counts[key] for game in games for player in game.players], bins=bins, density=True)
    ax.hist([game.winner.counts[key] for game in games], bins=bins, color='r', density=True, alpha=0.5)
    ax.set_xlabel(key)
    ax.set_title(key)


def plot_player_func(ax: Axes, games: List[Game], func: Callable[[Player], float], name: str,
                     bins: Optional[range] = None):
    ax.hist([func(player) for game in games for player in game.players], bins=bins, density=True)
    ax.hist([func(game.winner) for game in games], bins=bins, color='r', density=True, alpha=0.5)
    ax.set_xlabel(name)
    ax.set_title(name)


def plot_player_func_multi(ax: Axes, games: List[Game], func: Callable[[Player], Any], name: str,
                           bins: Optional[range] = None):
    ax.hist([val for game in games for player in game.players for val in func(player)], bins=bins, density=True)
    ax.hist([val for game in games for val in func(game.winner)], bins=bins, color='r', density=True, alpha=0.5)
    ax.set_xlabel(name)
    ax.set_title(name)
