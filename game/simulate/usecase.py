from math import ceil
from typing import List

from matplotlib.pyplot import subplots

from game.cards import UseCase
from game.gameconfig import GameConfig


def simulate_usecase_budgets(use_cases: List[UseCase], n_columns: int = 3):
    n_rows = int(ceil(len(use_cases) / n_columns))
    fig, axes = subplots(nrows=n_rows, ncols=n_columns, squeeze=False, figsize=(n_columns * 5, n_rows * 4))
    axes = [ax for row in axes for ax in row]
    for ax, use_case in zip(axes, use_cases):
        _plot_usecase(use_case, ax=ax)
    fig.tight_layout()


def _simulate_rolls(use_case: UseCase, team: int) -> int:
    game = GameConfig.load()
    position = 0
    rolls = 0
    while position <= game.phase_length * 4:
        rolls += 1
        position += use_case.progress(team=team)
    return rolls


def _plot_usecase(use_case: UseCase, ax=None):
    game = GameConfig.load()
    teams = list(range(max(game.min_team_size, use_case.handicap - 3), game.max_team_size + 1))
    data = [[_simulate_rolls(use_case, team) * team * 5 for _ in range(1000)] for team in teams]
    ax.boxplot(x=data, labels=teams, showfliers=False)
    ax.axhline(use_case.total_budget, label='budget')
    ax.set_ylim([0, 700])
    ax.set_title(f'Budget={use_case.total_budget}, Handicap={use_case.handicap}')
    ax.set_xlabel('team size')
    ax.set_ylabel('budget spent')
