from pandas import DataFrame

from game.player import Player


def simulate_single_game():
    player = Player()
    data = []
    large_data = []
    try:
        while not player.finished:
            player.turn()
            data.append(dict(
                team=player.team,
                completed=len(player.completed),
                handicap=player.handicap if player.use_case else 0,
                backlog=len(player.backlog),
                budget_cards=player.counts['budget_cards']
            ))
            large_data.append(dict(
                value=player.value,
                budget=player.budget,
            ))
    except RuntimeError as e:
        print(e)
    DataFrame(data).plot(figsize=(16, 5))
    DataFrame(large_data).plot(figsize=(16, 5))
