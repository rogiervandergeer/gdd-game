from random import randint
from typing import Optional

from game.config import GameObject


class Action(GameObject):

    def __init__(self, budget: float = 0, steps: int = 0, skip: int = 0, name: Optional[str] = None, ):
        super().__init__(name=name)
        self.budget = budget
        self.skip = skip
        self.steps = steps


class Budget(GameObject):

    def __init__(self, budget: float = 0, expenses: bool = False, team: int = 0, value: float = 0,
                 name: Optional[str] = None):
        super().__init__(name=name)
        self.budget = budget
        self.expenses = expenses
        self.team = team
        self.value = value


class Team(GameObject):

    def __init__(self, fee: float, team: int = 1, name: Optional[str] = None):
        super().__init__(name=name)
        self.fee = fee
        self.team = team


class UseCase(GameObject):

    def __init__(self, initial_budget: float, halfway_budget: float, immediate_value: float,
                 handicap: int, name: Optional[str] = None):
        super().__init__(name=name)
        self.initial_budget = initial_budget
        self.halfway_budget = halfway_budget
        self.immediate_value = immediate_value
        self.handicap = handicap

    @property
    def total_budget(self):
        return self.initial_budget + self.halfway_budget

    def progress(self, team: int) -> int:
        handicap = min(team - self.handicap, 3)
        if handicap < -3:
            raise TeamTooSmallError
        return max(randint(1, 6) + handicap, 0)


class TeamTooSmallError(RuntimeError):
    pass

