from collections import Counter
from random import choice, random
from typing import Callable, List, Optional

from game.cards import Action, Team, UseCase, Budget, TeamTooSmallError
from game.gameconfig import GameConfig


class Player:
    action_cards = Action.load()
    budget_cards = Budget.load()
    team_cards = Team.load()
    usecase_cards = UseCase.load()
    game = GameConfig.load()

    def __init__(self):
        self.budget = self.game.starting_budget
        self.position = -1
        self.max_position = -1
        self.team: int = self.game.starting_team_size
        self.backlog: List[UseCase] = []
        self.completed: List[UseCase] = []
        self.use_case = None
        self.value = 0
        self.skip = 0
        self.counts = Counter()

    @property
    def finished(self) -> bool:
        return len(self.completed) > 2

    @property
    def score(self) -> float:
        return self.value + self.budget

    @property
    def handicap(self) -> Optional[int]:
        if self.use_case:
            return self.team - self.use_case.handicap

    def turn(self):
        if self.finished:
            raise RuntimeError('Already finished!')
        else:
            if self.skip > 0:
                self.skip -= 1
            elif self.position == -1:
                self._handle_use_case()
            else:
                try:
                    self._handle_progress()
                except TeamTooSmallError:
                    self._handle_drop()
                    return
            self._handle_budget()

    def _handle_progress(self):
        steps = self.use_case.progress(team=self.team)
        if self.past_point(steps=steps, point=2 * self.game.phase_length):
            self.budget += self.use_case.halfway_budget
        for point in (1, 3):
            if self.past_point(steps=steps, point=point * self.game.phase_length):
                self._handle_team()
        self.position += steps
        self.max_position = max(self.max_position, self.position)
        if self.position > 4 * self.game.phase_length:
            self.value += self.use_case.immediate_value
            self.completed.append(self.use_case)
            self.use_case = None
            self.position = -1
            self.max_position = -1
        else:
            if random() < self.game.action_fraction:
                action_card: Action = choice(self.action_cards)
                self.position += action_card.steps
                self.max_position = max(self.position, self.max_position)
                self.skip = action_card.skip
                self.budget += action_card.budget

    def past_point(self, steps: int, point: int) -> bool:
        return self.max_position < point <= self.position + steps

    def _handle_team(self):
        team_card = choice(self.team_cards)
        if self.budget > 4 * team_card.fee and self.team < 8:
            self.team = min(self.team + team_card.team, 8)
            self.budget -= team_card.fee

    def _handle_budget(self):
        while self.budget < self.team * 5:
            if self.handicap and self.handicap < -2:
                self._handle_drop()
                return  # No need to pay team when giving up
            else:
                budget_card = choice(self.budget_cards)
                self.budget += budget_card.budget
                if budget_card.expenses:
                    self.budget += self.team * 5
                self.team += max(budget_card.team, self.game.min_team_size)
                self.value += budget_card.value
                self.counts.update(budget_cards=1)
        self.budget -= self.team * 5

    def _handle_drop(self):
        self.use_case = None
        self.position = -1
        self.counts.update(drop=1)

    def _handle_use_case(self):
        self.backlog.append(choice(self.usecase_cards))
        # Remove a use case if we have too many
        if len(self.backlog) > self.game.max_use_cases:
            self.backlog = self.backlog[1:]
        self.use_case = self._select_use_case(requirement=lambda uc: abs(self.team - uc.handicap) < 2)
        if not self.use_case and len(self.backlog) == self.game.max_use_cases:
            self.use_case = self._select_use_case(requirement=lambda uc: self.team - uc.handicap >= -3)
        if self.use_case:
            self.backlog.remove(self.use_case)
            self.position = 0
            self.budget += self.use_case.initial_budget

    def _select_use_case(self, requirement: Callable[[UseCase], bool]) -> Optional[UseCase]:
        passing = [use_case for use_case in self.backlog if requirement(use_case)]
        if len(passing) > 0:
            return choice(passing)

    def __repr__(self):
        return f'Player(pos={self.position}, budget={self.budget}, value={self.value}, team={self.team})'
