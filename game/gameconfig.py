from typing import Optional

from game.config import GameObject


class GameConfig(GameObject):

    def __init__(self, starting_budget: float, phase_length: int, action_fraction: float,
                 starting_team_size: int, min_team_size: int, max_team_size: int, max_use_cases: int):
        super().__init__()
        self.starting_budget = starting_budget
        self.phase_length = phase_length
        self.action_fraction = action_fraction
        self.starting_team_size = starting_team_size
        self.min_team_size = min_team_size
        self.max_team_size = max_team_size
        self.max_use_cases = max_use_cases

    @classmethod
    def load(cls, path: Optional[str] = None) -> 'GameConfig':
        return super().load(path)[0]
